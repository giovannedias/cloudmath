//
//  UI+Extensions.swift
//  CalcResults
//
//  Created by gio emiliano on 2019-09-25.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation
import UIKit


class calcPadButton : UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        backgroundColor = Colors().customBlue2
        layer.cornerRadius = 5
        layer.borderWidth = 1
        layer.borderColor = Colors().customBlue5.cgColor

    }
}


class goButton : UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        layer.cornerRadius = 5
        layer.borderWidth = 1
        layer.borderColor = Colors().customBlue5.cgColor
        backgroundColor = Colors().customBlue3

        
    }
}

class mainButton : UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        backgroundColor = .white
        layer.cornerRadius = 5
        layer.borderWidth = 1
        backgroundColor = Colors().customBlue3
        layer.borderColor = Colors().customBlue5.cgColor
        
        setTitleColor(Colors().customBlue5, for: .normal)
        
        titleLabel?.font =  Fonts().fontWith(size: 40)
        
        
        
    }
}


class ResultLabel : UILabel {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        textAlignment = .right
        backgroundColor = .clear
        
        
    }
}

class ResultView : UIView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
       
        backgroundColor = Colors().customBlue1
        layer.cornerRadius = 5
        layer.borderWidth = 1
        layer.borderColor = Colors().customBlue5.cgColor

        
    }
}


extension Double {
    func toString()->String {
        
        
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 8 //maximum digits in Double after dot (maximum precision)
        var s =  String(formatter.string(from: number) ?? "")
        if s.first == "." {
            s = "0"+s
        }
        
        return s
    }
    func toSimpleString()->String {
        
        
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2 //maximum digits in Double after dot (maximum precision)
        var s =  String(formatter.string(from: number) ?? "")
        if s.first == "." {
            s = "0"+s
        }
      
        
        return s
    }
}


extension UIViewController {
    func isLandscape()->Bool{
        if ( UIDevice.current.orientation == .landscapeLeft) || ( UIDevice.current.orientation == .landscapeRight){
            return true
            
        }
        
        return false
    }
}

extension UIView {
    func callRecursively(level: Int = 0, _ body: (_ subview: UIView, _ level: Int) -> Void) {
        body(self, level)
        subviews.forEach { $0.callRecursively(level: level + 1, body) }
    }
}
