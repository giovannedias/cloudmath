//
//  MainViewController.swift
//  CalcResults
//
//  Created by gio emiliano on 2019-10-02.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation
import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var appTitle: UILabel!
    @IBOutlet weak var playGameButton: mainButton!
    @IBOutlet weak var calculator: mainButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors().customBlue1
        
        
       
        appTitle.font = Fonts().fontWith(size: 40)
            
        appTitle.textAlignment = .center
        appTitle.text = "Fun Calc"
        appTitle.textColor = Colors().customBlue4
        
       

        
    }
    @IBAction func showGameViewController(_ sender: UIButton) {
        
        performSegue(withIdentifier: "SegueGame", sender: nil)
    }
    @IBAction func showCalculatorViewController(_ sender: UIButton) {
        performSegue(withIdentifier: "SegueCalculator", sender: nil)

    }
    
}

