//
//  CalcEquation.swift
//  CalcResults
//
//  Created by gio emiliano on 2019-10-02.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//



import Foundation
import UIKit


class Calculator {
    
    func calcWith(labelObject: UILabel, newText: String?, eq: CalcEquation) {
        
        guard let value = newText else { return }
        
        
        switch(value)
        {
        case "AC":
            eq.restart()
            labelObject.text = "0"
            
            break
            
        case ".":
            
            guard let text = labelObject.text else { return }
            
            if (text.contains("."))  { return }
            
            labelObject.text? += "."
            
            eq.setNum(num: text)
            
            break
            
        case "%":
            if let  resultLabel =  labelObject.text {
                eq.setPercentFrom(num: resultLabel)
                labelObject.text = eq.stringResult
            }
            break
        case "+","-","x","÷":
            
            if eq.isReadyForCalculate {
                
                eq.setOperator(op: value)
                
                labelObject.text = eq.stringResult
                
                return
                
            }
            
            eq.setOperator(op: value)
            
            break
            
        case "=":
            
            eq.flagOPChanged = true
            labelObject.text = eq.stringResult
            
            break
            
            
        case "⇦":
            
            if (labelObject.text?.count == 1) {
                labelObject.text? = "0"
                return
            }
            
            if (labelObject.text != "0") || (labelObject.text != nil) {
                
                labelObject.text?.removeLast()
            }
            
            break
            
        case "+/-":
            
            guard var text = labelObject.text else { return }
            if (text.first == "-")  {
                
                text.removeFirst()
                
            } else {
                
                text = "-" + text
                
            }
            
            labelObject.text = text
            eq.setNum(num: text)
            
            break
            
        case "0","1","2","3","4","5","6","7","8","9":
                if (labelObject.text == "0") { labelObject.text = "" }
            if (labelObject.text == "-0") { labelObject.text = "-" }

            
            if eq.flagOPChanged {
                
                labelObject.text = ""
                eq.flagOPChanged = false
            }
            
            labelObject.text? += value
            
            if let  resultLabel =  labelObject.text {
                eq.setNum(num: resultLabel)
            }
            
            break
            
        default:
            
            break
        }
        
        
        return
    }
    
}

class CalcEquation {
    
    var flagOPChanged:Bool = false
    var isXopen:Bool = true
    var isYopen:Bool = false
    
    var x:Double?
    var y:Double?
    
    
    func setNum(num : String){
        if self.isXopen {
            x = Double(num)
        } else {
            y = Double(num)
        }
        self.printEquation()
        
    }
    
    var result:Double?
    
    var stringResult : String {
        get{
            if (self.isReadyForCalculate) {
                if self.calculate() == false {
                    self.restart()
                    self.flagOPChanged = true

                    return "Error"
                }
                
            }
            
            guard let result = result else {return ""}
            
            return result.toString()
        }
    }
    
    var op:String?
    
    
    func setPercentFrom(num: String) {
        if  let p = Double(num) {
            x = p/100
            result = x
            op = nil
            y = nil
        }
        
    }
    
    func setOperator(op : String) {
        
        flagOPChanged = true
        if self.isXopen { self.isXopen = false}
        
        if self.hasOnlyTheFirst {
            self.op = op
            self.printEquation()
            
            return
        }
        if self.isReadyForCalculate {
            self.calculate()
            self.op = op
            
            self.printEquation()
            
            return
        }
        if self.hasOperator {
            self.op = op
            self.printEquation()
            
            return
        }
    }
    
    
    func initWith(x:Double, y:Double, op:String){
        
        self.x = x
        self.y = y
        self.op = op
        
    }
    
    func restart(){
        
        self.x = nil
        self.y = nil
        self.result = nil
        self.isXopen = true
        self.flagOPChanged = false
        
        self.printEquation()
        
    }
    
    
    
    func  calculate()->Bool {
        
        guard let x = self.x else { return false }
        guard let y = self.y else { return false }
        
        switch op {
        case "+":
            result = x + y
        case "-":
            result = x - y
        case "÷":
            if y==0 {return false}
            result = x / y
        case "x":
            result = x * y
            
        default:
            break
        }
        self.x = result
        self.y = nil
        self.printEquation()

        return true
    }
}




extension CalcEquation  {
    var hasOperator : Bool {
        get  {
            return (op != nil)
        }
    }
    var isReadyForCalculate : Bool {
        get  {
            return (x != nil) && (y != nil) && (op != nil)
        }
    }
    
    var hasFirstAndOperator : Bool {
        get  {
            return  (op != nil)
        }
    }
    var hasOnlyTheFirst : Bool {
        get  {
            return (x != nil) && (y==nil) && (op==nil)
        }
    }
    
    var hasOnlyTheSecond : Bool {
        get  {
            return (x == nil)
        }
    }
    
    var isEmpty : Bool {
        get  {
            return (x == nil) && (y == nil)
        }
    }
}


extension CalcEquation {
    
    func printEquation(){
        
        var strX = "nil"
        var strY = "nil"
        var strR = "nil"
        var strOP = "nil"
        
        
        
        if let fx = self.x  {
            strX = String(format: "%.2f", fx)
        }
        
        if let fy = self.y {
            
            strY = String(format: "%.2f", fy)
            
        }
        
        
        if let fresult = self.result {
            
            strR = String(format: "%.2f", fresult)
            
        }
        
        
        if let fOP = self.op {
            
            strOP = fOP
            
        }
        
        
        print("x: " + strX  + " op: " + strOP + " y: " + strY + " result: " + strR );
    }
    
}
