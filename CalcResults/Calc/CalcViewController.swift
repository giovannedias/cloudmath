//
//  CalcViewController.swift
//  CalcResults
//
//  Created by gio emiliano on 2019-10-02.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation
import UIKit

class CalcViewController: UIViewController {
    
    @IBOutlet weak var backgroundLabel: ResultView!
    
    @IBOutlet weak var labelResult: ResultLabel!
    
    var equation = CalcEquation()
    let calculator = Calculator.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       checkOrientation()
       
        self.title = "Calculator"
        view.backgroundColor = Colors().customBlue1
        backgroundLabel.backgroundColor = Colors().customBlue2


        labelResult.text = "0"

        
        
    }
  
   @IBAction func buttonTaped(_ sender: UIButton) {

        calculator.calcWith(labelObject: labelResult, newText: sender.titleLabel?.text, eq: equation)
        
        return
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        checkOrientation()
    }
    
    func checkOrientation(){
        if self.isLandscape() {
            
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            
                      
        } else {
            
            
            backgroundLabel.callRecursively { (subview, level) in
                for constraint in subview.constraints {
                    print(String(repeating: " ", count: level),  constraint.constant)
                }
            }
            
            
            self.navigationController?.setNavigationBarHidden(false, animated: true)

        }

    }
}
