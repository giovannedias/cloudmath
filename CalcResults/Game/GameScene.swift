//
//  GameScene.swift
//  CalcResults
//
//  Created by gio emiliano on 2019-09-17.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//


import Foundation
import GameplayKit


protocol GameSceneProtocol {
    
    func gameHasFinished()
    
    
}
class GameScene: SKScene {
    
    var gameDelegate: GameSceneProtocol?
    private let floor = Floor()
    
    var difficulty : Difficulty = .easy

    var score = 0
    
    var stop : Bool = false

    override func didMove(to view: SKView) {
        
        
        let wait3   = SKAction.wait(forDuration:  3)
        let spawn   = SKAction.run {self.createEquation() }
        let action = SKAction.sequence([wait3, spawn])


        let forever = SKAction.repeatForever(action)
        self.run(forever)
        
        physicsWorld.gravity = CGVector(dx: 0, dy: -1.62)


        
    }
    
    override func sceneDidLoad() {
        
        addChild(floor)
        
        let leftWall = Wall().initWith(side: .left)
        let rightWall = Wall().initWith(side: .right)

        
        addChild(leftWall)
        addChild(rightWall)

    }
    
    func createEquation() {
        
        if (stop == true) { return }
        
       let randomEquation = Equation.init(circleOfRadius: 100)
        
        if score > 5 {
            difficulty = .medium
        }
        
        if score > 10 {
            difficulty = .hard
        }
        randomEquation.initWith(difficullty: difficulty)
        
        self.addChild(randomEquation)
    }
    
    func checkResultWith(result: String)->Bool {
        
        print("check result")

        var countRight = 0
        
        for child in self.children {
            
            if let equation = child as? Equation {
                
                if equation.result == result {
                    countRight += 1
                   
                    equation.disapear()
                   // child.removeFromParent()
                }
            }
         
        }
        
        if countRight > 0 {
            score += countRight
            print("\(countRight) equations deleted")
            return true
        }
        return false
    }
    
    func removeAllEquations(){
        
        for child in self.children {
            
            if let equation = child as? Equation {
                
                equation.removeFromParent()
                self.gameDelegate?.gameHasFinished()
                
            }
        }
        
    }
    override  func update(_ currentTime: TimeInterval) {
        
        if self.children.count > 20 {
            removeAllEquations()
            stop = true
        }
    }
    
    func restart(){
        stop = false
        difficulty = .easy
        score = 0
    }
}


public enum Difficulty {
    case hard
    case medium
    case easy
}
