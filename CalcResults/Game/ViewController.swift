//
//  ViewController.swift
//  CalcResults
//
//  Created by gio emiliano on 2019-09-17.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//


//NAME
//STUDENT
//DESCRIPTION
//VERSION HISTORY
//v1 added funcionallity


import UIKit
import GameKit

class ViewController: UIViewController {
    
    @IBOutlet weak var backgroundScore: UIView!
    @IBOutlet weak var backgroundResultLabel: ResultView!
    @IBOutlet weak var highScore: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var result: UILabel!
    @IBOutlet weak var spriteKitView: SKView!
    private var scene : GameScene?
    private var count = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
       checkOrientation()
        
     
        
        self.title = "Game"
        
        highScore.font = Fonts().fontWith(size: 20)
        score.font = Fonts().fontWith(size: 20)


     
        view.backgroundColor = Colors().customBlue1
        
        backgroundResultLabel.backgroundColor = Colors().customBlue2
        backgroundScore.backgroundColor = Colors().customBlue3


        scene = SKScene(fileNamed: "GameScene") as! GameScene
        scene!.scaleMode = .aspectFill
        scene!.gameDelegate = self
        scene?.backgroundColor = Colors().customBlue2
        spriteKitView.presentScene(scene)
        
        /*
        spriteKitView.ignoresSiblingOrder = true
        spriteKitView.showsFPS = true
        spriteKitView.showsNodeCount = true
        spriteKitView.showsPhysics = true*/
        
        
        clear()
        updateHighScore()
        updateScore()
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        backgroundResultLabel.addGestureRecognizer(swipeRight)
        backgroundResultLabel.isUserInteractionEnabled = true
        
        
        // function which is triggered when handleTap is called
      
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == .right {
            result.text = "0"
        }

    }
    

    @IBAction func buttonTaped(_ sender: Any) {
        
        if let button = sender as? UIButton {
            
            let number = button.titleLabel!.text
            
            
            guard var text = result.text  else { return }
            
            if text == "0" && number != "."{
                
                text = ""
            }
            
            
            if number == "." && text.contains("."){ return }
            
            if number == "+/-" {
                
                if (text.first == "-")  {
                    
                   result.text!.removeFirst()
                } else {
                    
                    result.text = "-" + result.text!

                }

            } else {
                result.text = text + number!

            }
            
            
        }
        
    }
    
    @IBAction func go(_ sender: Any) {
        

        if  scene?.checkResultWith(result: result.text!) == true {
            count = count + 1
            updateScore()
        }
        
        clear()

    }
    
    func updateHighScore(){
        highScore.text = "Highest score: " + localHighScoreAllTime()
    }
    
    func updateScore(){
        score.text = "Current score: " + String(count)
    }
    
    func clear(){
        result.text = "0"
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        checkOrientation()
    }
    
    func checkOrientation(){
        if self.isLandscape() {
            
            self.navigationController?.setNavigationBarHidden(true, animated: true)

        } else {
            self.navigationController?.setNavigationBarHidden(false, animated: true)

        }
    }
    
}




extension ViewController : GameSceneProtocol {
    
    func gameHasFinished() {
        if checkHighestScore(currentScore: count) {
            
            let alert = UIAlertController(title: title, message: "YAY new record! ", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.scene?.restart()
            }))
            self.present(alert, animated: true)

            updateHighScore()
        } else {
            
            
            let alert = UIAlertController(title: title, message: "Good Job! Keep trying!", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
                self.scene?.restart()
                
            }))
            self.present(alert, animated: true)

        }
        count = 0
        updateScore()
    }
}


