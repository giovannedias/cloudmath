//
//  ViewController+Extensions.swift
//  CalcResults
//
//  Created by gio emiliano on 2019-09-25.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation
import UIKit


extension ViewController {
    
    func saveNewHighScore(score: String) {
        UserDefaults.standard.set(score, forKey: "HighScore") //setObject

    }
    
    func localHighScoreAllTime()->String {
        
        if let highScore  =  UserDefaults.standard.string(forKey: "HighScore") {
            return  highScore
        }

        return "0"
    }
    
    func checkHighestScore(currentScore : Int)->Bool {
        let hightScoreAllTime = localHighScoreAllTime()
        
        guard let inthightScoreAllTime = Int(hightScoreAllTime) else { return false}

        if currentScore > inthightScoreAllTime {
                saveNewHighScore(score: String(currentScore))
                return true
        }
        
       
        
        return false
    }
}


extension ViewController {
    
    func simpleAlertWith(message : String, andTitle: String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
}

extension UILabel {
    func deleteByGesture(){
        


    }
    
}

