//
//  Nodes.swift
//  CalcResults
//
//  Created by gio emiliano on 2019-09-25.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation
import GameKit


public class Floor : SKShapeNode {
    override public init() {
        super.init()
        self.position = CGPoint(x: 0, y: -270)//CGPoint(x: 0, y: 300)
        self.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: UIScreen.main.bounds.width*2, height: 30))
        self.physicsBody?.isDynamic = false
        physicsBody?.restitution = 0
        
        
        physicsBody?.categoryBitMask = 0b0001
        
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


public class Wall : SKShapeNode {
    
    public enum Side {
        case left
        case right
    }
    
    
    func initWith(side : Side) -> SKShapeNode {
        self.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 30, height: UIScreen.main.bounds.width*2))
        self.physicsBody?.isDynamic = false
        physicsBody?.restitution = 0
        physicsBody?.categoryBitMask = 0b0001
        
        self.side(s: side)
        
        return self
        
        
    }
    
    override init() {
        super.init()
    }
    
    func side(s: Side){
        switch s {
        case .left:
            self.position = CGPoint(x: -350, y: -200)//CGPoint(x: 0, y: 300)
            
        case .right:
            self.position = CGPoint(x: 350, y: -200)//CGPoint(x: 0, y: 300)
            
        default:
            break
        }
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
