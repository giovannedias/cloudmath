//
//  Equation.swift
//  CalcResults
//
//  Created by gio emiliano on 2019-09-17.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation
import GameKit



class Equation : SKShapeNode {
    
 
    
    enum Opperations: CaseIterable  {
        
        case addition
        case subtraction
        case multiplication
        case division
        
        var description : String {
            switch self {
                
            case .addition: return "+"
            case .subtraction: return "-"
            case .multiplication: return "*"
            case .division: return "/"

            }
        }
    }
    
    var result : String?
    var x : Double = 0
    var y : Double = 0
    var op = Opperations.addition
    
    var readable : String {
        
        return String(Int(x)) + op.description + String(Int(y))
    }

    
    var difficullty : Difficulty = .easy
    
    let fade = SKAction.fadeAlpha(to: 0, duration: 1)

    
    func initWith(difficullty: Difficulty) {
        
        
        self.position = randomPosition()
        self.difficullty = difficullty
        
        createEquation()
     

        let label : SKLabelNode = SKLabelNode(fontNamed: Fonts().customFont())
        label.text = readable
        label.fontSize = 80
        
        // label.position = CGPointMake(circle.frame.midX, circle.frame.midY)
        label.fontColor = .black
        
        self.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 120, height: 80))//SKPhysicsBody(circleOfRadius: 20)
        self.physicsBody!.affectedByGravity = true
        self.physicsBody!.collisionBitMask = 0
        physicsBody?.collisionBitMask = 0b0001
        


        self.addChild(label)
        
    }
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createEquation(){
        
        var operationResult = 0.0

        
        switch self.difficullty {
        case .hard:
            self.fillColor = Colors().hardRed
            
            x = Double(Int.random(in: 1 ... 10))
            y = Double(Int.random(in: 1 ... Int(x)))
            op = [Opperations.multiplication,Opperations.division][Int.random(in: 0 ... 1)]

            break
            
        case .medium:
            self.fillColor = Colors().mediumYellow
            x = Double(Int.random(in: 0 ... 10))
            y = Double(Int.random(in: 0 ... Int(x)))
            op = [Opperations.addition,Opperations.subtraction,Opperations.multiplication][Int.random(in: 0 ... 2)]

            break
            
        case .easy:
            self.fillColor = Colors().easyWhite

            x = Double(Int.random(in: 0 ... 10))
            y = Double(Int.random(in: 0 ... Int(x)))
            op = [Opperations.addition,Opperations.subtraction][Int.random(in: 0 ... 1)]
            
        }
        
        
        switch op {
            
        case .addition:
            operationResult = Double(x + y)
            
        case .subtraction:
            operationResult = Double(x - y)
            
        case .multiplication:
            operationResult = Double(x * y)
            
        case .division:
            if (y==0) {
                y = 1
            }
            operationResult = Double(x / y)
            
        }
        
        
        result = operationResult.toSimpleString()
    }
    
    func moveDown(){
        
        //self.run(SKAction.moveBy(x:0, y: -2.0, duration: 0.1))
      //  print(self.position)

    }
    
    func disapear(){
        self.run(fade,completion: {
            self.removeFromParent()

        })
    }
    
    func randomPosition()->CGPoint  {
        
        let randomX = Int.random(in: -250 ... 250)
        
        return CGPoint(x: randomX, y: 320)
    }

}


