//
//  Colors.swift
//  CalcResults
//
//  Created by gio emiliano on 2019-10-02.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation
import UIKit

class Colors {
    
    var easyWhite : UIColor {
        get {
            return UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
        }
    }
    var mediumYellow : UIColor {
        get {
            return UIColor.init(red: 255/255, green: 255/255, blue: 0/255, alpha: 0.5)
        }
    }
    var hardRed : UIColor {
        get {
            return UIColor.init(red: 255/255, green: 0/255, blue: 0/255, alpha: 0.5)
        }
    }
    var customBlue1 : UIColor {
        get {
            return UIColor.init(red: 26/255, green: 175/255, blue: 255/255, alpha: 1)
        }
    }
    var customBlue2 : UIColor {
        get {
            return UIColor.init(red: 28/255, green: 142/255, blue: 253/255, alpha: 1)
        }
    }
    //45 104 252
    var customBlue3 : UIColor {
        get {
            return UIColor.init(red: 45/255, green: 104/255, blue: 252/255, alpha: 1)
        }
    }
    
    //52 46 201
    var customBlue4 : UIColor {
        get {
            return UIColor.init(red: 52/255, green: 46/255, blue: 201/255, alpha: 1)
        }
    }
    
   // 27 14 137
    var customBlue5 : UIColor {
        get {
            return UIColor.init(red: 27/255, green: 14/255, blue: 137/255, alpha: 1)
        }
    }
}


