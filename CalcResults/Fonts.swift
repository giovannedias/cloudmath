//
//  Fonts.swift
//  CalcResults
//
//  Created by gio emiliano on 2019-10-02.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation
import UIKit
import GameKit


class Fonts {
    
    let fontName : String = "DKCoolCrayon"
    
    func customFont()->String{
        return fontName
    }
    func fontWith(size: CGFloat) -> UIFont {
        guard let customFont = UIFont(name: fontName, size: size) else {
            return .systemFont(ofSize: size)
        }
        
        return customFont
    }
    
    func adjustLabelFontSizeToFitRect(labelNode:SKLabelNode, rect:CGRect) {
        
        // Determine the font scaling factor that should let the label text fit in the given rectangle.
        let scalingFactor = min(rect.width / labelNode.frame.width, rect.height / labelNode.frame.height)
        
        // Change the fontSize.
        labelNode.fontSize *= scalingFactor
        
        // Optionally move the SKLabelNode to the center of the rectangle.
        labelNode.position = CGPoint(x: rect.midX, y: rect.midY - labelNode.frame.height / 2.0)
    }

    
    func showAllFonts(){
        for family in UIFont.familyNames.sorted() {
            let names = UIFont.fontNames(forFamilyName: family)
            print("Family: \(family) Font names: \(names)")    
         }
    }
}
